# Netstat Parser

The Netstat Parser is a Python script that processes the output of the netstat command and organizes the information into a structured JSON format. It provides a command-line interface (CLI) for easy usage.

## Prerequisites

To run this script, ensure that you have the following:

- Python 3.x installed on your system.
- Access to the netstat command.

## Usage

`Note:` Only the TCP part is outputed and saved as JSON

```bash
netstat | python3 cli.py -o my-json.output
----------------------------------------------------
----------------------- TCP ------------------------
----------------------------------------------------
{
  "ESTABLISHED": [
    {
      "Proto": "tcp",
      "Recv-Q": "0",
      "Send-Q": "0",
      "Local Address": "192.168.1.100:12345",
      "Foreign Address": "203.0.113.10:80",
      "(state)": "ESTABLISHED"
    },
    ...
  ],
  "TIME_WAIT": [],
  "FIN_WAIT_2": [],
  "OTHER": [
    {
      "Proto": "tcp",
      "Recv-Q": "0",
      "Send-Q": "0",
      "Local Address": "192.168.1.100:54321",
      "Foreign Address": "198.51.100.20:443",
      "(state)": "UNKNOWN_STATE"
    },
    ...
  ]
}
----------------------------------------------------
----------------------- UDP4 -----------------------
----------------------------------------------------
{
  "UDP4": [
    "udp4       0      0  192.168.1.100:53       0.0.0.0:*"
  ]
}
----------------------------------------------------
----------------------- UDP6 -----------------------
----------------------------------------------------
{
  "UDP6": []
}
----------------------------------------------------
----------------------- UDP46 ----------------------
----------------------------------------------------
{
  "UDP46": []
}

```


