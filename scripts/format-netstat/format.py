import json
import re

def process_netstat_logs(netstat_input, output = None):
    title_keys = ["Proto", "Recv-Q", "Send-Q", "Local Address", "Foreign Address", "(state)"]
    established = []
    timewait = []
    finwait2 = []
    other = []

    udp6_all = []
    udp4_all = []
    udp46_all = []

    for line in netstat_input:
        print(line)
        if "tcp" in line:
            print(line)
            list_line = re.split('\s',line)
            list_line_clean = []
            mapped_line = {}
            for elem in list_line:
                if "" != elem:
                    list_line_clean.append(elem)
            for i in range(len(title_keys)-1):
                mapped_line[title_keys[i]] = list_line_clean[i]


            
            print(list_line_clean)
            if "ESTABLISHED" in line: 
                established.append(mapped_line)
            elif "TIME_WAIT" in line:
                timewait.append(mapped_line)
            elif "FIN_WAIT_2" in line:
                finwait2.append(mapped_line)
            else:
                other.append(mapped_line)
        elif "udp6" in line:
            udp6_all.append(line)
        elif "udp4" in line:
            udp4_all.append(line)
        elif "udp46" in line:
            udp46_all.append(line)
        if "Active Multipath Internet connections" in line:
            break

    tcp = {"ESTABLISHED": established, "TIME_WAIT": timewait, "FIN_WAIT_2": finwait2, "OTHER": other}
    udp4 = {"UDP4": udp4_all}
    udp6 = {"UDP6": udp6_all}
    udp46_all = {"UDP46": udp46_all}

    print("----------------------------------------------------")
    print("----------------------- TCP ------------------------")
    print("----------------------------------------------------")
    print(json.dumps(tcp, indent=2,))
    print("----------------------------------------------------")
    print("----------------------- UDP4 -----------------------")
    print("----------------------------------------------------")
    print(json.dumps(udp4, indent=2))
    print("----------------------------------------------------")
    print("----------------------- UDP6 -----------------------")
    print("----------------------------------------------------")
    print(json.dumps(udp6, indent=2))
    print("----------------------------------------------------")
    print("----------------------- UDP46 ----------------------")
    print("----------------------------------------------------")
    print(json.dumps(udp6, indent=2))

    if output is not None:
        with open(output, 'w') as file:
            json.dump(tcp, fp=file, indent=2)


