import argparse
import sys
from format import process_netstat_logs

if __name__ == "__main__":
    # Create the argument parser
    parser = argparse.ArgumentParser(description="Analyse netstat logs")

    # Add the command-line arguments
    parser.add_argument("-f", "--filepath", required=False, help="Path to the netstat logs file")
    parser.add_argument("-o", "--output", required=False, help="Output path to save the result")

    # Parse the arguments
    args = parser.parse_args()

    if args.filepath is not None:
        # Call the function with the file path and output path
        with open(args.filepath, "r") as input_file:
            process_netstat_logs(input_file, args.output)
    else:
        # Read input from pipe
        pipe_input = sys.stdin.readlines()
        # Call the function with the piped input
        process_netstat_logs(pipe_input, args.output)
