import json

def extract_json_objects_from_log(log_file_path):
    json_objects = []
    
    with open(log_file_path, 'r') as log_file:
        log_data = log_file.read()
        log_lines = log_data.splitlines()
        
        for line in log_lines:
            # Assuming each JSON object is on a separate line in the log file
            try:
                json_object = json.loads(line)
                json_objects.append(json_object)
            except json.JSONDecodeError:
                pass
    
    return json_objects
