# Log File JSON Extractor

This Python script allows you to extract JSON objects from a log file. It can be useful for debugging request logs or analyzing JSON data within the logs.

## Prerequisites

- Python 3

## Usage

`python3 cli.py <log_file_path>`

Replace `<log_file_path>` with the path to your log file. The script will extract all JSON objects from the log file and print them to the console.
