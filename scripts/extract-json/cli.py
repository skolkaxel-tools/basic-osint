import sys
import json
from extract import extract_json_objects_from_log

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: python cli.py <log_file_path>')
        sys.exit(1)
    
    log_file_path = sys.argv[1]
    extracted_json_objects = extract_json_objects_from_log(log_file_path)

    # Iterate over the extracted JSON objects
    for json_object in extracted_json_objects:
        print(json.dumps(json_object, indent=4))  # Print the JSON object (formatted)
