#!/bin/bash

# Check if the URL file argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <url_file>"
    exit 1
fi

# Get the URL file argument
url_file="$1"

# Define sensitive patterns you want to search for
sensitive_patterns="-e 'password' -e 'api_key' -e 'token' -e 'secret' -e 'access_key' -e 'private_key' -e 'auth_key' -e 'credentials'"
#echo "grep -i $sensitive_patterns"
# Loop through each URL in the file

while IFS= read -r url; do
    echo "Checking URL: $url"
    
    # Fetch the content of the URL, decompress, and search for sensitive patterns
    curl -s --compressed "$url" | grep -iE $sensitive_patterns | tee -a sensitive.txt | grep -iE $sensitive_patterns && echo "Potential sensitive information found!"
done < "$url_file"
