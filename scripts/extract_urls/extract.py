from bs4 import BeautifulSoup
import re

# Define your domain
my_domain = "https://kibana-coverage.elastic.dev/2023-08-02T06:41:00Z/jest-combined"
	
# Read the HTML file
with open('resources/index.html', 'r') as file:
    html_content = file.read()

# Parse the HTML using Beautiful Soup
soup = BeautifulSoup(html_content, 'html.parser')

# Find all <a> tags and extract href values
for a_tag in soup.find_all('a'):
    href = a_tag.get('href')
    if href:
        # Append domain to the extracted path
        path = re.sub(r'^/', '', href)  # Remove leading slash if present
        full_url = f"{my_domain}/{path}"
        full_url = re.sub('https://istanbul.js.org/', '', full_url)
        print(full_url)

#print('https://istanbul.js.org/')
