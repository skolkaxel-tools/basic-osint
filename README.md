# Basic Osint

Tools good to use

* subfinder
* Burp suite
* jq
* sed
* curl

## subfinder

Subfinder is used to find subdomains to any subdomains.

Install it by running
```bash
brew install subfinder
```

### Example

```bash
subfinder -d khanacademy.com -oJ > khan.json
```

This will search for all (I think) subdomains to khanacademy.com and output it to a json file named khan.json.

```json
{"host":"www.khanacademy.com","input":"khanacademy.com","source":"alienvault"}
```

In this case we only found one subdomain.

## Burp suite

Can be used to intercept, replay, and change content of web traffic. And much much more.

### Example

TBD

## jq

jq is a command line tool used for JSON processing

install it by running

```bash
brew install jq
```

### Example

Lets say we have the following json string.

```txt
'{"fruit":{"name":"apple","color":"green","price":1.20}}'
```

To prettify we can run 

```bash
boy@guy99:$ echo '{"fruit":{"name":"apple","color":"green","price":1.20}}' | jq '.'
{
  "fruit": {
    "name": "apple",
    "color": "green",
    "price": 1.2
  }
}
boy@guy99:$
```

We can also apply this filter directly to a JSON file. Say we have the initial JSON store in fruit.json.

```bash
jq '.' fruit.json
```



## sed

sed, a stream editor.
https://www.gnu.org/software/sed/manual/sed.html

*available by default with most linux based OSs*


Similar to grep, but more powerful as sed can find, filter, and modify text patterns, where as grep can only filter and find.


## curl

Curl is a command-line tool and a library for making requests to various protocols, including HTTP, HTTPS, FTP, SMTP, and more. It is widely used for automating and testing network communication. Curl supports a wide range of options and features, making it a versatile tool for retrieving and sending data over the internet.

### Basic Usage

To make a simple GET request using curl, you can use the following command:

```shell
curl [URL]
```

Replace `[URL]` with the actual URL you want to access. By default, curl will output the response body to the console.

### Examples

1. Making a GET request and saving the response to a file:

```shell
curl -o output.html https://example.com
```

This command retrieves the content of the given URL and saves it as `output.html` in the current directory.

2. Sending a POST request with data:

```shell
curl -X POST -d "name=John&age=30" https://api.example.com/users
```

In this example, a POST request is made to `https://api.example.com/users`, sending the data `name=John&age=30` in the request body.

3. Setting custom request headers:

```shell
curl -H "Content-Type: application/json" https://api.example.com/data
```

Here, the `-H` option is used to set the `Content-Type` header to `application/json` before making the GET request.

These examples illustrate just a few of the many ways curl can be used to interact with different APIs and protocols.


