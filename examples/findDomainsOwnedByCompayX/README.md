# Find toplevel domain for a company

List all domains belongin to companyX.
```bash
boy@guy99 ~ % curl -s https://crt.sh\?o\=companyX\&output\=json > companyX.json
```
The content of companyX.json.
```bash
boy@guy99 ~ % cat companyX.json
[{"issuer_ca_id":1399,"issuer_name":"C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert SHA2 Extended Validation Server CA","common_name":"www.companyX.com","name_value":"companyX Yolo AB","id":652757903,"entry_timestamp":"2018-08-18T14:35:27.308","not_before":"2018-08-08T00:00:00","not_after":"2019-10-02T12:00:00","serial_number":"059ad66cff5f0fe79b02e463ff9567af"},{"issuer_ca_id":1399,"issuer_name":"C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert SHA2 Extended Validation Server CA","common_name":"www.companyX.com","name_value":"companyX Yolo AB","id":637829457,"entry_timestamp":"2018-08-08T15:31:05.432","not_before":"2018-08-08T00:00:00","not_after":"2019-10-02T12:00:00","serial_number":"059ad66cff5f0fe79b02e463ff9567af"},{"issuer_ca_id":1399,"issuer_name":"C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert SHA2 Extended Validation Server CA","common_name":"myzone.companyX.com","name_value":"companyX Yolo AB","id":26503073,"entry_timestamp":"2016-08-01T11:21:43.734","not_before":"2016-07-11T00:00:00","not_after":"2018-08-29T12:00:00","serial_number":"045b04ac5a1a9f0ae9744abdae986ccd"},{"issuer_ca_id":1399,"issuer_name":"C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert SHA2 Extended Validation Server CA","common_name":"www.companyX.com","name_value":"companyX Yolo AB","id":24333572,"entry_timestamp":"2016-07-11T10:12:03.098","not_before":"2016-07-11T00:00:00","not_after":"2018-08-29T12:00:00","serial_number":"045b04ac5a1a9f0ae9744abdae986ccd"},{"issuer_ca_id":1399,"issuer_name":"C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert SHA2 Extended Validation Server CA","common_name":"yolo.companyX.com","name_value":"companyX Yolo AB","id":7610868,"entry_timestamp":"2014-12-30T17:55:08.276","not_before":"2014-05-30T00:00:00","not_after":"2016-08-02T12:00:00","serial_number":"0e3671d3505528826edc45521163f870"},{"issuer_ca_id":1399,"issuer_name":"C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert SHA2 Extended Validation Server CA","common_name":"*.companyX.com","name_value":"companyX Yolo AB","id":5680257,"entry_timestamp":"2014-11-22T17:09:49.045","not_before":"2014-05-30T00:00:00","not_after":"2016-08-02T12:00:00","serial_number":"0f5a0b970c95504e3dbcd0f226b8be10"},{"issuer_ca_id":33,"issuer_name":"C=US, O=DigiCert Inc, OU=www.digicert.com, CN=DigiCert High Assurance EV CA-1","common_name":"www.companyX.com","name_value":"companyX Yolo AB","id":4265073,"entry_timestamp":"2014-06-04T12:24:42.525","not_before":"2014-05-30T00:00:00","not_after":"2016-08-02T12:00:00","serial_number":"0e5360047c9f3de4a7f353d0bf484d5b"}]
```


## Filter the data
Now we can clean it up using `jq`

```bash
boy@guy99% cat companyX.json | jq -r '.[].common_name'
www.companyX.com
www.companyX.com
myzone.companyX.com
www.companyX.com
*.companyX.com
yolo.companyX.com
www.companyX.com

```

Filter out the wildcard domain.

```bash
boy@guy99 ~ % cat companyX.json | jq -r '.[].common_name' | sed 's/\*//g' | wc -l
8
```

Remove all duplicates.

```bash
boy@guy ~% x cat companyX.json | jq -r '.[].common_name' | sed 's/\*//g' | sort -u
myzone.companyX.com
www.companyX.com
yolo.companyX.com
```


Remove the subdomains of the filtered URLs, and remove duplicates again.

```bash
boy@guy99 % cat companyX.json | jq -r '.[].common_name' | sed 's/\*//g' | sort -u | rev | cut -d "." -f 1,2 | rev| sort -u
companyX.com
```


## Alternative tools

You can also use tools if you dont want to use your CLI. There are probably more, but three I know of is whoxy API,  domainparser, and uriparser.

### Whoxy API

Our WHOIS API returns consistent and well-structured WHOIS data in XML & JSON format. Returned data contain parsed WHOIS fields that can be easily understood by your application. Along with WHOIS API, we also offer WHOIS History API and Reverse WHOIS API.

https://www.whoxy.com/reverse-whois/

| Service                           | Price                   |
|-----------------------------------|-------------------------|
| 1000 WHOIS Lookup API Queries     | $2                      |
| 1000 WHOIS History API Queries    | $5                      |
| 1000 Reverse WHOIS API Queries    | $10                     |
| Newly Registered Domains Database | $495                    |
| Whois Database [494 Million Domains] | $9,500               |

### URIparser 

uriparser is a strictly RFC 3986 compliant URI parsing and handling library written in C89 ("ANSI C"). uriparser is cross-platform, fast, supports both char and wchar_t strings, and is licensed under the New BSD license.

There are a number of applications, libraries and hardware using uriparser, as well as bindings and 3rd-party wrappers. uriparser is packaged in major distributions.

https://uriparser.github.io/doc/api/latest/


